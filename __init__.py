import st3m.run
from st3m import InputState
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.utils import tau
from st3m.ui.view import BaseView
import random
import leds
import math
from st3m.ui import colours


class Colours():

    def __init__(self,r,g,b,petal):
        self.r = r
        self.g = g
        self.b = b
        self.petal = petal

    

class Game(BaseView):

    def __init__(self):
        super().__init__()
        self.colours = [
            Colours(1,0,0,0),
            Colours(0,0,1,2),
            Colours(1,1,0,4),
            Colours(0.643, 0.051, 0.82,6),
            Colours(0.094, 0.788, 0.282,8)
        ]
        self.PETALPUN = 0
        self.PETALSIN = 2
        self.PETALKEL = 4
        self.PETALVIO = 6
        self.PETALVIH= 8
        self.pressed = False
        self.score = 0
    
    def seuraava(self):
        self.alku = random.choice(self.colours)
        

    def think(self, ins: InputState, delta_ms: int):

        if self.pressed == False:
            for c in self.colours:
                petal=ins.captouch.petals[c.petal]

                if petal.pressed:
                    if self.alku == c:
                        self.score += 1
                        self.seuraava()
                        self.pressed=True
                    else:
                        self.vm.push(EndScreen(self.score))

        petalpun=ins.captouch.petals[self.PETALPUN]
        petalsin=ins.captouch.petals[self.PETALSIN]
        petalkel=ins.captouch.petals[self.PETALKEL]
        petalvio=ins.captouch.petals[self.PETALVIO]
        petalvih=ins.captouch.petals[self.PETALVIH]
        
        if self.pressed:
            if not petalpun.pressed and not petalsin.pressed and not petalkel.pressed and not petalvio.pressed and not petalvih.pressed:
                self.pressed=False

        
    def on_enter(self, vm):
        super().on_enter(vm)
        self.alku = random.choice(self.colours)
        self.score = 0

    def draw (self, ctx: Context) -> None:
        ctx.rgb(self.alku.r,self.alku.g,self.alku.b).rectangle(-120, -120, 240, 240).fill()

    #Värit Punainen:1,0,0 Sininen: 0,0,1 Keltainen: 1,1,0 Violetti: 0.643, 0.051, 0.82 Vihreä: 0.094, 0.788, 0.282
        

class EndScreen(BaseView):

    def __init__(self,score):
        super().__init__()
        self.kehu = ""
        self.score = score

    def draw (self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.font_size = 25
        
        ctx.rgb(0.5, 0, 0.5)
        ctx.font = "Camp Font 2"
        ctx.text_align = ctx.CENTER
        ctx.move_to(0, -50)
        ctx.text(self.kehu)

        ctx.move_to(0, -20)
        ctx.text("Your score is:")
        ctx.move_to(0, 0)
        ctx.text(f"{self.score}")


    def on_enter(self, vm):
        super().on_enter(vm)
        self.kehu = random.choice(["Nice try!","Good Job!","You rock!","Purrfect!","Awesome!","OwO","UwU"])






        


class ColorGame(Application):

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.input.buttons.app.middle.pressed:
            self.vm.push(Game())

    def on_enter(self, vm):
        super().on_enter(vm)

        #green leds
        leds.set_all_rgb(0,1,0)

        #turn off between leds
        for i in [4,12, 20, 28, 36]:
            leds.set_rgb(i, 0, 0, 0)
        
        #red leds
        for i in range(0,4):
            leds.set_rgb(i, 1, 0, 0)
        for i in range(37,40):
            leds.set_rgb(i, 1, 0, 0)

        #blue leds
        for i in range(5,12):
            leds.set_rgb(i, 0, 0, 1)
        
        #yellow leds
        for i in range(13,20):
            leds.set_rgb(i, 1, 1, 0)
        
        #purple leds
        for i in range(21,28):
            leds.set_rgb(i, 1, 0, 1)
        

        leds.update()

    def draw(self, ctx: Context) -> None:
        
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.font_size = 25
        
        ctx.rgb(0.5, 0, 0.5)
        ctx.font = "Camp Font 2"
        ctx.text_align = ctx.LEFT
        ctx.wrap_left = -95.0
        ctx.wrap_right = 95.0
        ctx.move_to(-95, -50)
        ctx.text("Welcome to Color Game!")

        ctx.text_align = ctx.LEFT
        ctx.wrap_left = -95.0
        ctx.wrap_right = 95.0
        ctx.move_to(-95, -10)
        ctx.text("Your goal is to press the petal that matches the color shown on screen.")

        ctx.text_align = ctx.CENTER
        ctx.move_to(0, 70)
        ctx.text("Good luck!")

        ctx.text_align = ctx.CENTER
        ctx.move_to(0, 90)
        ctx.text("Press X to play")


if __name__ == '__main__':
    st3m.run.run_app(ColorGame, "/sd/apps/Color-Game")